# README #

### Installing virtualenv ###

* Setup the python virtual environment:``` python3 -m venv venv``` And then activate it: ```source venv/bin/activate```

### Install requirements.txt ###

* ```pip intall -r requirements.txt```

### Run ###
* The results be returned as json ```python main.py --f 'Form W-2' 'Form 1095-C'```
* Download all PDFs available in the 2015-2020 range: ```python main.py --f 'Form W-2' 'Form 1095-C' --yr 2015-2020```