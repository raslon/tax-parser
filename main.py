import os
import time
from functools import partial

import bs4 as bs4
import requests
import argparse


bs = partial(bs4.BeautifulSoup, features='lxml')


def createParser():
    parser = argparse.ArgumentParser(description='A tutorial of argparse!')
    parser.add_argument("--f", type=str, nargs="+", help="List tax form names 'f' variable,"
                                                         " (--f 'Form W-2' 'Form 1095-C')")
    parser.add_argument("--yr", type=str, help="Specify the name of the tax"
                                               " form and range of years to download"
                                               " all PDFs available in that range"
                                               " (--f Form W-2 --yr 2017-2019)")
    return parser
def get_html(url):
    while True:
        req = requests.get(url)
        if req.status_code==200:
            return req
        else:
            time.sleep(5)

def get_table(form_number):
    trs = []
    indexOfFirstRow = 0
    while True:
        url = 'https://apps.irs.gov/app/picklist/list/priorFormPublication.html?indexOfFirstRow={}&sortColumn=sortOrder&value={}&criteria=formNumber&resultsPerPage=200&isDescending=false'.format(indexOfFirstRow,form_number)
        raw_html = get_html(url)
        soup = bs(raw_html.text)
        if not soup.find('p', {'id': 'errorText'}):
            table = soup.find('table', {'class': 'picklist-dataTable'})
            trs += table.find_all('tr')
            indexOfFirstRow = indexOfFirstRow + 200
        else:
            break
    return trs

def get_data(trs, form_number, year_b, year_e):
    max_year = 0
    min_year = 0
    title = ''
    for tr in trs:
        if not tr.find('th'):
            form = tr.find('td', {'class': 'LeftCellSpacer'}).text.strip()
            if form == form_number:
                title = tr.find('td', {'class': 'MiddleCellSpacer'}).text.strip()
                year = int(tr.find('td', {'class': 'EndCellSpacer'}).text.strip())
                pdf_link = tr.find('td', {'class': 'LeftCellSpacer'}).find('a')['href']
                if year_range:

                    if year in range(year_b, year_e+1):
                        raw_html = get_html(pdf_link)
                        if not os.path.exists(form):
                            os.mkdir(form)
                        with open('{}/{} - {}.pdf'.format(form,form,year), 'wb') as f:
                            f.write(raw_html.content)
                else:
                    if year >= max_year:
                        max_year = year
                    else:
                        min_year = year
    if not year_range:
        return {
            "form_number": form,
            "form_title": title,
            "min_year": min_year,
            "max_year": max_year}



def run(forms, year_range=None):

    data = []
    try:
        year_b = int(year_range.split('-')[0])
        year_e = int(year_range.split('-')[-1])
    except:
        print('Year range error!')
        return
    for form in forms:
        trs = get_table(form)
        if not trs:
            print('"{}" not found'.format(form))
        else:
            if forms:
                if year_range:
                    get_data(trs, form, year_b, year_e)
                else:
                    data.append(get_data(trs, form))
    if data:
        print(data)


if __name__ == '__main__':
    parser = createParser()
    args = parser.parse_args()
    forms = args.f
    year_range = args.yr
    if forms:
        run(forms, year_range)







